/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import Splash from './Components/Instagram/SplashScreen/Splash';
import {name as appName} from './app.json';

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {thisScreen: 'Splash'};
        setTimeout(()=>{
            this.setState({thisScreen: 'App'})
        }, 3000)
    }
    render(){
        const {thisScreen} = this.state
        let splashScreen = thisScreen === 'Splash' ? <Splash /> : <App />
        return splashScreen 
    }
}
AppRegistry.registerComponent(appName, () => Main);

import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Gambar from './Components/userProfile/Gambar';
import Bio from './Components/userProfile/Bio';
import Tumbal from './Components/userProfile/Tumbal';
import Navigator from './Components/Instagram/Navigation/Navigation';
import Home from './Components/Instagram/Home';


const App: () => React$Node = () => {
  return (
    <>
        {/* <View style={{flex:1, alignItems: 'flex-start'}}>
          <View style={{width: 450, height: 750, backgroundColor: 'red'}} />
          <View style={{width: 450, height: 750, backgroundColor: 'yellow'}} />
        </View> */}
        <Navigator />
    </>
  );
};


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     justifyContent: "center"
//   },
//   text: {
//     fontWeight: "bold",
//     fontSize: 20
//   },
//   item1: {
//     backgroundColor: "#feffcb"
//   },
//   item2: {
//     backgroundColor: "#c6394d"
//   },
//   item3: {
//     backgroundColor: "#49beb7"
//   },
//   item4: {
//     backgroundColor: "#240041"
//   }
// });

export default App;

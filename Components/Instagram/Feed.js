import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
} from 'react-native';
import { Card, Icon } from 'react-native-elements';
import { Body, Row } from 'native-base';


const Feed = props => {
    return (
        <>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: 'white', height: 460 }}>
                    <View>
                        <Card>
                            <View style={{ paddingRight: "40%" }}>
                                <Body>
                                    <Text style={{ fontFamily: 'monospace' }}>{props.nama}</Text>
                                    <Text>{props.lokasi}</Text>
                                </Body>
                                <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={props.profile} />
                            </View>
                            <Card.Divider />
                            <View>
                                <Image style={{ width: 340, height: 300 }} source={props.src} />
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{margin:3 }} source={require('../../Gambar/heart.png')} />
                                    <Image style={{margin:3 }} source={require('../../Gambar/speech-bubble.png')} />
                                    <Image style={{margin:3 }} source={require('../../Icons/send.png')} />
                                </View>
                                <Text style={{fontWeight: "bold"}}>{props.account}</Text>
                                <Text>{props.caption}</Text>
                            </View>
                        </Card>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Feed;
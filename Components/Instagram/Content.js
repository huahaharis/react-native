import React from 'react';
import { View, Text, TouchableOpacity, TextInput, Button, TouchableHighlight } from 'react-native';
import { Icon } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

class Content extends React.Component {
    render() {
        return (
            <>
                <View style={{ flex: 1 }}>
                    <View style={{flexDirection: "row", paddingTop: 5, paddingRight:12 }}>
                        <View style={{ paddingRight: 10, paddingLeft: 15, paddingTop: 4 }}>
                            <Icon name="ios-search-sharp" />
                        </View>
                        <TextInput style={{ paddingTop: 4, fontSize: 20, paddingRight:14, paddingLeft:14 }} placeholder="Cari"
                    maxLength= {40} />
                        <View style={{ paddingLeft: 245, paddingTop: 3 }}>
                            <Icon name="ios-grid-outline" />
                        </View>
                    </View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{ alignItems: "center" }}>
                        <View style={{ flexDirection: "row", paddingLeft: 15 }}>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10 }}>
                                    <Icon name="ios-logo-instagram" />
                                    <Text>IGTV</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10 }}>
                                    <Icon name="ios-barbell-outline" />
                                    <Text>Sport</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10, alignItems: 'center' }}>
                                    <Icon name="ios-car-sport-outline" />
                                    <Text>Automotive</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10, alignItems: 'center'}}>
                                    <Icon name="ios-cart-outline" />
                                    <Text>Shopping</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10, alignItems: "center" }}>
                                    <Icon name="ios-book-outline" />
                                    <Text>Knowledge</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10, alignItems: "center" }}>
                                    <Icon name="ios-airplane-outline" />
                                    <Text>Holiday</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10 }}>
                                    <Icon name="ios-game-controller-outline" />
                                    <Text>Game</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { }}>
                                <View style={{ paddingRight: 10, paddingLeft: 10 }}>
                                    <Icon name="ios-fast-food-outline" />
                                    <Text>Food</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </>
        )
    }
}

export default Content;
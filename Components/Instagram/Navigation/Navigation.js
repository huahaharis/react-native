import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../Home';
import Search from '../Screen/Search';
import Like from '../Screen/Like';
import Gallery from '../Screen/Gallery';
import Profile from '../Screen/Profile';
import { TouchableOpacity, Text, View } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {Icon} from 'native-base';
import DropDownPicker from 'react-native-dropdown-picker';


const Stack = createStackNavigator();

function Navigation(){

    handleChoosePhoto = () => {
        const options = {
            noData: true
        };
    
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('response =', response);
        });
    };
    
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen 
                name="Home" 
                component={Home}
                options={{ 
                    
                headerLeft: () => (
                    <TouchableOpacity onPress={handleChoosePhoto}>
                    <View style={{padding: 10}}>
                        <Icon name="camera-outline"/>
                    </View>
                    </TouchableOpacity>
                ), 
                headerTitle: () => (
                    <View>
                        <Text style={{fontWeight: "bold", fontSize: 20, fontStyle: "italic", padding: 79}}>Instagram</Text>
                    </View>
                ),
                headerRight: () => (
                    <TouchableOpacity>
                        <View style={{padding: 10}}>
                        <Icon name="ios-paper-plane-outline"/>
                        </View>
                    </TouchableOpacity>
                )
                }} />
                <Stack.Screen 
                name="Search" 
                component={Search}
                options={{headerShown: false}} />
                <Stack.Screen 
                name="Gallery" 
                component={Gallery}
                options={{headerShown: false}} />
                <Stack.Screen 
                name="Like" 
                component={Like}
                options={{headerLeft: null,
                    headerTitle: "Activity"
                }} />
                <Stack.Screen 
                name="Profile" 
                component={Profile}
                options={{
                    headerLeft: () => (
                        <DropDownPicker 
                        items={[
                            {label: 'USA', value: 'usa', icon: () => <Icon name="flag" size={18} color="#900" />, hidden: true},
                            {label: 'UK', value: 'uk', icon: () => <Icon name="flag" size={18} color="#900" />},
                            {label: 'France', value: 'france', icon: () => <Icon name="flag" size={18} color="#900" />},
                        ]}
                        />
                    ),
                    headerTitle: null,
                    }}/>            
                
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Navigation;
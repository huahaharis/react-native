import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Search from './Screen/Search';


const BottomNav = props => {
    const {navigation} = props;
    return (
        <>
        <View style={{height: 55, flexDirection: 'row', backgroundColor: 'white'}}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() =>
                    props.navigation.navigate('Home')}>
                    <View>
                        <Image source={require('../../Gambar/home.png')} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() =>
                    props.navigation.navigate(Search)}>
                    <View>
                        <Image source={require('../../Gambar/search.png')} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() =>
                    this.navigation.navigate('Gallery')}>
                    <View>
                        <Image source={require('../../Gambar/plus-sign.png')} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() =>
                    this.navigation.navigate('Like')}>
                    <View>
                        <Image source={require('../../Gambar/heart.png')} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() =>
                    this.navigation.navigates('Profile')}>
                    <View>
                        <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={require('../../Gambar/jonson.jpg')} />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
        </>
    )
}

export default BottomNav;
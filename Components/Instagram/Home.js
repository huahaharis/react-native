import { Container, Icon } from 'native-base';
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import Feed from '.././Instagram/Feed';
import ThumbNail from '../Instagram/ThumbNail';



const Home = (props) => {
    const {navigation} = props;
    return (
        <>
        <ScrollView>
            <ThumbNail />
                <View style={{ flex: 1 }}>
                    <Feed
                        profile={require('../../Gambar/jonson.jpg')}
                        nama="Dwayne Jhonson"
                        lokasi="Hatimu uwwuuuuuuu"
                        account="Dwayne Jhonson"
                        src={require('../../Gambar/lagiFitnes.jpeg')}
                        caption="Korona boleh galak, tapi kita gaboleh kalah galak!!"
                    />
                    <Feed
                        profile={require('../../Gambar/profilescarlet.jpeg')}
                        nama="Scarlet Johanson"
                        lokasi="Banjar, Kalimantan Barat"
                        account="Scarlet Johanson"
                        src={require('../../Gambar/prewed.jpeg')}
                        caption="Upsss sorry bentar lagi married"
                    />
                    <Feed
                        profile={require('../../Gambar/jonson.jpg')}
                        nama="Dwayne Jhonson"
                        lokasi="Lembang, Jawa Barat"
                        account="Dwayne Jhonson"
                        src={require('../../Gambar/duluvsskrng.jpg')}
                        caption="Maafin aku yang dulu sayang <3"
                    />
                    <Feed
                        profile={require('../../Gambar/profilescarlet.jpeg')}
                        nama="Scarlet Johanson"
                        lokasi="Pantai Kuta, Bali Barat"
                        account="Scarlet Johanson"
                        src={require('../../Gambar/nikah.jpg')}
                        caption="Kan dibilang apa"
                    />
                </View>
            </ScrollView>
            <View style={{ height: 55, flexDirection: 'row', backgroundColor: 'white' }}>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Home')}>
                        <View>
                            <Icon name="ios-home-outline"/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Search')}>
                        <View>
                            <Icon name="ios-search-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Gallery')}>
                        <View>
                            <Icon name="ios-add-circle-outline"/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Like')}>
                        <View>
                            <Icon name="ios-heart-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Profile')}>
                        <View>
                            <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={require('../../Gambar/jonson.jpg')} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}


export default Home;

const style = StyleSheet.create({
    bottomnav: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center' 
    }
})
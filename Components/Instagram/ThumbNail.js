import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
} from 'react-native';
import { Container, Icon } from 'native-base';



const ThumbNail = () => {
    return(
        <>
        <View style={{height: 100}}>
            <View style={style.container}>
                <Text style={{fontWeight: "bold", fontSize: 19}}>
                    Stories
                </Text>
                <View style={{flexDirection: "row"}}>
                 <Icon name="ios-play-circle"/>   
                 <Text style={{paddingTop: 6, fontWeight: "bold"}}>Watch All</Text>
                </View>    
            </View>
            <View style={{paddingBottom:10}}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{alignItems: "center", paddingStart: 10, paddingEnd: 5}}>
                    <Image style={style.image} source={require('../../Gambar/jonson.jpg')}/>
                    <Image style={style.image} source={require('../../Gambar/siganteng.jpeg')}/>
                    <Image style={style.image} source={require('../../Gambar/vindisel.jpeg')}/>
                    <Image style={style.image} source={require('../../Gambar/park.jpeg')}/>
                    <Image style={style.image} source={require('../../Gambar/jason.jpeg')}/>
                    <Image style={style.imageRead} source={require('../../Gambar/markus.jpeg')}/>
                    <Image style={style.imageRead} source={require('../../Gambar/kiko.jpeg')}/>
                </ScrollView>
            </View>
        </View>
        </>
    )
}

export default ThumbNail;


const style = StyleSheet.create({
    container: {
       flex:1, 
       justifyContent: "space-between", 
       flexDirection: "row", 
       alignItems: 'center', 
       paddingHorizontal: 7, 
       paddingBottom: 10, 
       paddingTop: 10
      },
    image: {
        borderRadius: 60, 
        height: 66, 
        width: 66, 
        borderWidth: 2, 
        borderColor: 'violet', 
        marginRight:5
    },
    imageRead: {
        borderRadius: 60, 
        height: 66, 
        width: 66, 
        borderWidth: 2, 
        borderColor: 'violet', 
        marginRight:5, 
        opacity: 0.6, 
        borderColor: '#DCDCDC'
    }
})
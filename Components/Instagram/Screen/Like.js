import { Icon } from 'native-base';
import React from 'react';
import {
    Button,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';


const Like = (props) => {
    const { navigation } = props;
    return (
        <>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, paddingRight: 20 }}>
                    <Text style={{ fontSize: 24, fontWeight: "bold" }}>This Week</Text>
                    <View style={{ paddingTop: 20, flexDirection: "row" }}>
                        <Image style={{ height: 56, width: 56, borderRadius: 40 }} source={require('../../../Gambar/rati.jpeg')} />
                        <Text style={{ paddingTop: 20, paddingLeft: 15 }}>Muhammad Raafi start following you.</Text>
                        <Text style={{ paddingLeft: 5, paddingTop: 20, color: 'grey' }}>7 Days</Text>
                    </View>
                </View>
                <View style={{ paddingBottom: 40 }}>
                    <Text style={{ fontSize: 24, fontWeight: "bold" }}>Earlier</Text>
                    <View style={{ paddingTop: 20, flexDirection: "row" }}>
                        <Image style={{ height: 56, width: 56, borderRadius: 40 }} source={require('../../../Gambar/tobat.jpeg')} />
                        <Text style={{ paddingTop: 20, paddingLeft: 15 }}>Maria Ozawaaaa start following you.</Text>
                        <Text style={{ paddingLeft: 25, paddingTop: 20, color: 'grey' }}>4 Days</Text>
                        <View style={style.container}>
                            <Button
                                title="Ikuti" />
                        </View>
                    </View>
                    <View style={{ paddingTop: 10, flexDirection: "row", paddingRight: 10 }}>
                        <Image style={{ height: 56, width: 56, borderRadius: 40 }} source={require('../../../Gambar/sakura.jpg')} />
                        <View>
                            <Text style={{ paddingTop: 1, paddingLeft: 17, fontWeight: "bold" }}>Stefany Sakura</Text>
                            <View style={{paddingLeft: 10}}>
                                <Text style={{ color: "blue" }}> @dwaynethehulk</Text>
                                <Text>1 Day</Text>
                            </View>
                        </View>
                        <View style={{ paddingRight: 10 }}>
                            <View>
                            <Text>mention you on comments:</Text>
                            <Text> hahahaha you so handsome</Text>
                            </View>
                        </View>
                        <View style={style.container}>
                            <Button
                                title="Ikuti" />
                        </View>
                    </View>
                </View>
                <View style={{ paddingBottom: 120 }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>Recommend For You</Text>
                    <View style={{ paddingTop: 20, flexDirection: "row" }}>
                        <Image style={{ height: 56, width: 56, borderRadius: 40 }} source={require('../../../Gambar/anya.jpg')} />
                        <Text style={{ paddingTop: 20, paddingLeft: 15, fontWeight: "bold" }}>Anya Geraldine</Text>
                        <View style={{paddingLeft: 187, paddingTop: 10}}>
                            <Button
                                title="Ikuti" />
                        </View>
                    </View>
                    <View style={{ paddingTop: 20, flexDirection: "row" }}>
                        <Image style={{ height: 56, width: 56, borderRadius: 40 }} source={require('../../../Gambar/ayunda.jpg')} />
                        <Text style={{ paddingTop: 20, paddingLeft: 15, fontWeight: "bold" }}>Maudy Ayunda</Text>
                        <View style={{paddingLeft: 187, paddingTop: 10}}>
                            <Button
                                title="Ikuti" />
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ height: 55, flexDirection: 'row', backgroundColor: 'white' }}>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Home')}>
                        <View>
                            <Icon name="ios-home-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Search')}>
                        <View>
                            <Icon name="ios-search-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Gallery')}>
                        <View>
                            <Icon name="ios-add-circle-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Like')}>
                        <View>
                            <Icon name="ios-heart-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigates('Profile')}>
                        <View>
                            <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={require('../../../Gambar/jonson.jpg')} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}


export default Like;

const style = StyleSheet.create({
    bottomnav: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        width: 55,
        borderRadius: 25,
        height: 45,
        paddingTop: 8,
        justifyContent: 'center',
        paddingLeft: 5
    }
})
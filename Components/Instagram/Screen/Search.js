import React, { useEffect } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    View,
    Text,
    Image,
} from 'react-native';
import { Icon } from 'native-base';
import Content from '../Content.js';
import Card from '../Card.js';
import { LogBox } from 'react-native';


const Search = props => {
    const { navigation } = props;

    useEffect(() => {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    }, [])


    return (
        <>
            <ScrollView>
                <View style={{ flex: 1 }}>
                    <Content />
                    <SafeAreaView>
                        <Card />
                    </SafeAreaView>
                </View>
            </ScrollView>
            <View style={{ height: 55, flexDirection: 'row', backgroundColor: 'white' }}>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Home')}>
                        <View>
                            <Icon name="ios-home-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Search')}>
                        <View>
                            <Icon name="ios-search-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Gallery')}>
                        <View>
                            <Icon name="ios-add-circle-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Like')}>
                        <View>
                            <Icon name="ios-heart-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigates('Profile')}>
                        <View>
                            <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={require('../../../Gambar/jonson.jpg')} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
}


export default Search;

const style = StyleSheet.create({
    bottomnav: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
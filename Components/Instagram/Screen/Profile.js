import { Container, Icon } from 'native-base';
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';


const Profile = (props) => {
    const { navigation } = props;
    return (
        <>
            <View style={{ flex: 1 }}>
                <Text>halo</Text>
            </View>
            <View style={{ height: 55, flexDirection: 'row', backgroundColor: 'white' }}>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Home')}>
                        <View>
                            <Icon name="ios-home-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Search')}>
                        <View>
                            <Icon name="ios-search-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Gallery')}>
                        <View>
                            <Icon name="ios-add-circle-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Like')}>
                        <View>
                            <Icon name="ios-heart-outline" />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={style.bottomnav}>
                    <TouchableOpacity onPress={() =>
                        navigation.navigate('Profile')}>
                        <View>
                            <Image style={{ borderRadius: 20, height: 32, width: 32 }} source={require('../../../Gambar/jonson.jpg')} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </>


    )
}

export default Profile;

const style = StyleSheet.create({
    bottomnav: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
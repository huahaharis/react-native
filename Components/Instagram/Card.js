import React, { Component } from "react";
import { ScrollView } from "react-native-gesture-handler";
import MasonryList from "react-native-masonry-list";



class TampilGambar extends Component {
    render() {
        return (
            
                <MasonryList
                    images={[
                        // Can be used with different image object fieldnames.
                        // Ex. source, source.uri, uri, URI, url, URL
                        {
                            source: require("../../Gambar/mukbang.jpeg"),
                            width: 300, height: 450
                        },
                        {
                            source: require("../../Gambar/kucingnakal.jpeg"),
                            width: 300, height: 450
                        },
                        {
                            source: require("../../Gambar/bola.jpg"),
                            width: 700, height: 450
                        },
                        {
                            source: require("../../Gambar/gawatbos.jpg"),
                            width: 700, height: 450
                        }, {
                            source: require("../../Gambar/lord.jpeg"),
                            width: 700, height: 450
                        }, {
                            source: require("../../Gambar/memar.jpg"),
                            width: 700, height: 500
                        }, {
                            source: require("../../Gambar/sunda.jpg"),
                            width: 700, height: 450
                        }, {
                            source: require("../../Gambar/wowo.jpg"),
                            width: 700, height: 450
                        }, {
                            source: require("../../Gambar/kimi.jpg"),
                            width: 600, height: 450
                        }, {
                            source: require("../../Gambar/inyourarea.jpeg"),
                            width: 300, height: 450
                        }, {
                            source: require("../../Gambar/kawin.jpg"),
                            width: 400, height: 450
                        }, {
                            source: require("../../Gambar/ramen.jpeg"),
                            width: 60, height: 45
                        }, {
                            source: require("../../Gambar/onepiece.jpeg"),
                            width: 60, height: 45
                        }, {
                            source: require("../../Gambar/cupangdlsini.jpg"),
                            width: 60, height: 45
                        },
                    ]}
                />
            
        );
    }
}
export default TampilGambar;
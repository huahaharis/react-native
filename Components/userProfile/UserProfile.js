import React from 'react';
import { View, ScrollView } from 'react-native';
import Username from './Username';
import Gambar from './Gambar';
import Bio from './Bio';
import Tumbal from './Tumbal';

const UserProfile = () => {
    return (
        <>
            <ScrollView horizontal>
                <View style={{ alignItems: 'center'}}>
                    <Gambar src={require('../../Gambar/bleach.jpeg')} />
                    <Tumbal />
                    <Bio />
                </View>
            </ScrollView>
        </>
    );
};

export default UserProfile;
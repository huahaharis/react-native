import React from 'react';
import {Text, View} from 'react-native';

class Bio extends React.Component {
    constructor(props){
        super(props)
        this.state={
            hobi: 'Gelut', 
            bakat: 'Nyari lele',
            pengalaman: 'Ngancurin Housen & Suzuran'
        }
    }


        render(){
            return(
                <>
                <View style={{alignItems: 'center'}}>
                    <Text>Hobi Gue adalah {this.state.hobi}</Text>
                    <Text>Bakat Gue yaitu {this.state.bakat}</Text>
                    <Text>Pengalaman Gue {this.state.pengalaman}</Text>
                </View>
                </>
            )
        }
    
    }


export default Bio;
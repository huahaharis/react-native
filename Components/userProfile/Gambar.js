import React from 'react';
import {View, Image} from 'react-native';

const Gambar = props => {
    return(
        <Image style={{
            width: 250, 
            height: 250,
            borderWidth: 1, 
            borderRadius: 250/2}} source={props.src}/>
        
    );
};

export default Gambar;
import React from 'react';
import {Text, View} from 'react-native';

class Username extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            nama: 'Ichigo Serizawa'
        }
    }

    updateNama(){
        if (this.state.nama == 'Rindaman Abdulah') {
            this.setState({
                nama: 'Ichigo Serizawa'
            })
        } else (this.setState ({
            nama: 'Rindaman Abdulah'
        })
    )}

    render(){
        return(
            <View>
                <Text onPress={() => this.updateNama()}>{this.state.nama}</Text>
            </View>
        )
    }
}

export default Username;